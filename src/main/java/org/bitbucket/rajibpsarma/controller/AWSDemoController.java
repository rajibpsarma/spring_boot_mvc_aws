package org.bitbucket.rajibpsarma.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AWSDemoController {

	@RequestMapping("/")
	public String showHomePage() {
		return "home";
	}
}
