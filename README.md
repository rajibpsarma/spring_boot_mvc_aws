# Spring Boot MVC app deployed to AWS

It's a simple demo app that just displays a home page.

It's used in AWS in the following ways:

* Code checked into AWS CodeCommit
* Code picked up by CodeBuild to compile, test & build the jar, by using buildspec.yml
* Deployed the jar by Elastic BeanStalk to EC2
* The above steps are managed by CodePipeline.
* The deployed app is available here: http://3.128.134.69/






